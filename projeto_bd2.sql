CREATE DATABASE LocadoraVeiculos;

create table cidade (
id_cidade smallint not null,
nome varchar(100) not null,
id_estado varchar (11) not null,
primary key (id_cidade)
)
go

create table cor (
	id_cor varchar(11)NOT NULL,
	cor_nome varchar(45) NOT NULL
);

CREATE TABLE locacao (
  id_locacao varchar(11) NOT NULL,
  id_cliente varchar(11) DEFAULT NULL,
  id_veiculo varchar(11) DEFAULT NULL,
  km_inicial float DEFAULT NULL,
  km_final float DEFAULT NULL,
  data datetime DEFAULT NULL,
 descricao varchar DEFAULT NULL
);

CREATE TABLE cliente (
  id_cliente varchar (11) NOT NULL,
  nome varchar(45) DEFAULT NULL,
  celular varchar(45) DEFAULT NULL,
  logradouro varchar(45) DEFAULT NULL,
  numero varchar(45) DEFAULT NULL,
  bairro varchar(45) DEFAULT NULL,
  cpf varchar(11) DEFAULT NULL,
  id_cidade varchar(11) DEFAULT NULL
);

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO cliente (id_cliente, nome, celular, logradouro, numero, bairro, cpf, id_cidade) VALUES
(1, 'Joe Montana', '(47)99999-9999', 'Rua sem saida', '122A', 'Centro', '123.456.789-10', 50),
(2, 'Joe Satriani', '(47)99999-9999', 'Rua sem saida', '122A', 'Centro', '123.456.789-10', 50),
(3, 'David Gilmour', '(47)99999-1111', 'Rua sem saida', '122b', 'Centro','999.456.789-10', 60),
(4, 'Roger Waters', '(47)99999-2222', 'Rua sem saida', '123A', 'Centro',  '888.456.789-10', 70),
(5, 'Nick Mason', '(47)99999-3333', 'Rua sem saida', '332A', 'Centro', '777.456.789-10', 999),
(6, 'Doro Pesch', '(47)99999-4444', 'Rua sem saida', '342', 'Centro', '666.456.789-10', 555),
(7, 'Tarja Turunen', '(47)99999-5555', 'Rua sem saida', '322D', 'Centro', '555.456.789-10', 888),
(8, 'Pepe Legal', '(47)99999-6666', 'Rua sem saida', '322F', 'Centro', '444.456.789-10', 50),
(9, 'Joelma do Calipso', '(47)99999-4444', 'Rua sem saida', '342', 'Centro','666.456.789-10', 555),
(10, 'Gretchen', '(47)99999-4444', 'Rua sem saida', '342', 'Centro', '666.456.789-10', 555),
(11, 'Pequeno gafanhoto', '(47)99999-7777', 'Rua sem saida', '333', 'Centro', '333.456.789-10', 777);

INSERT INTO locacao (id_locacao, id_cliente, id_veiculo, km_inicial, km_final, data, descricao) VALUES
(1, 1, 1, 500, 6000, '2021-09-14 18:43:39', ''),
(2, 2, 2, 500, 5555, '2021-09-14 18:43:39', ''),
(3, 3, 3, 4444, 6000, '2021-09-14 18:43:39', ''),
(4, 3, 5, 33, 6000, '2021-09-14 18:43:39', ''),
(5, 4, 4, 4444, 6000, '2021-09-14 18:43:39', ''),
(7, 5, 5, 500, 44444, '2021-09-14 18:43:39', ''),
(8, 6, 6, 500, 4443, '2021-09-14 18:43:39', ''),
(9, 7, 7, 500, 6200, '2021-09-14 18:43:40', ''),
(10, 8, 7, 500, 3333, '2021-09-14 18:43:40', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_veiculo`
--

CREATE TABLE tipo_veiculo (
  id_tipo_veiculo varchar(11) NOT NULL,
  nome varchar(45) DEFAULT NULL,
  valor_km float DEFAULT NULL
);

--
-- Extraindo dados da tabela `tipo_veiculo`
--

INSERT INTO tipo_veiculo (id_tipo_veiculo, nome, valor_km) VALUES
(1, 'Passeio 1.0', 0.3),
(2, 'Passeio 1.6', 0.5),
(3, 'Utilitário', 1.1),
(4, 'Van', 1.3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculo`
--

CREATE TABLE veiculo (
  id_veiculo varchar(11) NOT NULL,
  nome varchar(45) DEFAULT NULL,
  id_cor varchar(11) DEFAULT NULL,
  id_modelo varchar(11) DEFAULT NULL,
  id_tipo_veiculo varchar(11) DEFAULT NULL
);

--
-- Extraindo dados da tabela `veiculo`
--

INSERT INTO veiculo (id_veiculo, nome, id_cor, id_modelo, id_tipo_veiculo) VALUES
(1, 'Veiculo 1', 1, 1, 1),
(2, 'Veiculo 2', 2, 2, 2),
(3, 'Veiculo 3', 2, 3, 3),
(4, 'Veiculo 4', 3, 4, 4),
(5, 'Veiculo 5', 4, 5, 1),
(6, 'Veiculo 6', 5, 6, 2),
(7, 'Veiculo 7', 1, 7, 3),
(8, 'Veiculo 8', 2, 8, 4),
(9, 'Veiculo 9', 3, 1, 1),
(10, 'Veiculo 10', 4, 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculo_marca`
--

CREATE TABLE veiculo_marca(
  id_marca varchar(11) NOT NULL,
  nome varchar(45) DEFAULT NULL
);

--
-- Extraindo dados da tabela `veiculo_marca`
--

INSERT INTO veiculo_marca (id_marca, nome) VALUES
(1, 'Volks'),
(2, 'GM'),
(3, 'Ford'),
(4, 'Audi'),
(5, 'Ferrari');

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculo_modelo`
--

CREATE TABLE veiculo_modelo (
  id_modelo varchar(11) NOT NULL, 
  nome varchar(45) DEFAULT NULL,
  id_marca varchar(11) DEFAULT NULL
);

--
-- Extraindo dados da tabela `veiculo_modelo`
--

INSERT INTO veiculo_modelo (id_modelo, nome, id_marca) VALUES
(1, 'Gol', 1),
(2, 'Fusca', 1),
(3, 'Saveiro', 1),
(4, 'Ônibus', 1),
(5, 'Pampa', 3),
(6, 'KA', 3),
(7, 'Chevette', 3),
(8, 'Agile', 2),
(9, 'Corsa', 2),
(10, 'Spin', 2),
(11, 'A3', 4),
(12, 'F50', 5),
(13, 'F40', 5);

ALTER TABLE cidade
ADD PRIMARY KEY (id_cidade);
CREATE INDEX id_estado_idx ON cidade(id_estado);


ALTER TABLE cliente
ADD PRIMARY KEY (id_cliente);
CREATE INDEX id_cidade_idx ON cidade(id_cidade);

ALTER TABLE cor
ADD PRIMARY KEY (id_cor);

ALTER TABLE estado
ADD PRIMARY KEY (id_estado);

ALTER TABLE locacao
ADD PRIMARY KEY (id_locacao),
CREATE INDEX id_cliente_idx ON locacao(id_cliente);
CREATE INDEX id_veiculo_idx ON locacao(id_veiculo);

CREATE PROCEDURE locacao
@id_cliente varchar (11)
as
begin transaction
	insert into Cliente)
	if(@@rowcount > 0) 
	begin
		insert into id_cliente
		values(@id_cliente, id_veiculo)
		if(@@rowcount > 0)/
		begin
			commit transanction
		else
		begin
			rollback transanction
		end
	end
	else
	begin
		rollback transanction
	end

CREATE PROCEDURE veiculo
@id_cliente varchar (11)
as
begin transaction
	insert into id_cliente)
	if(@@rowcount > 0) 
	begin
		insert into id_cliente
		values(@id_cliente, id_veiculo)
		if(@@rowcount > 0)/
		begin
			commit transanction
		else
		begin
			rollback transanction
		end
	end
	else
	begin
		rollback transanction
	end 
	
////VIEWS///

select count(*) from locacao;

/*
Podemos dar apelidos aos nomes das tabelas, desta forma facilita a digitalização
*/
select count(*) from locacao loc, cliente c where loc.id_cliente = c.id_cliente;

/*
subquery  ou subconsulta
*/
select 
	loc.*, (select nome from cliente c where loc.id_cliente = c.id_cliente)
from 
	locadora loc, 
    cliente c
where 
	loc.id_cliente = c.id_cliente;
    
#
select 
	loc.*, c.nome
from 
	locadora loc, cliente c;
    
# 
select 
	loc.*, 
    c.nome
from 
	locadora loc, 
    cliente c
where 
	loc.id_cliente = c.id_cliente;

/*
É possível darmos um nome a nossa lógica da subquery
*/
select 
	loc.*, (select nome from c where loc.id_cliente = c.id_cliente) "nome_alias_apelido_tabela"
from 
	locadora loc, 
    cliente c
where 
	loc.id_cliente = c.id_cliente;

#    
select 
	loc.*, (select nome from cliente c where loc.id_cliente = c.id_cliente) "nome_alias_apelido_tabela"
from 
	locadora loc, 
    cliente c;

#    
select 
	*
from 
	locadora loc, 
    cliente c;

#
select
	loc.*,
    c.nome,
    v.nome
from
	locacao loc,
    cliente c,
	veiculo v
where
	loc.id_veiculo = v.id_veiculo
	and loc.id_cliente = c.id_cliente;
    
#
select
	loc.*,
    c.nome,
    v.nome
from
	locacao loc,
    cliente c,
	veiculo v
where
	loc.id_veiculo = v.id_veiculo
	and loc.id_cliente = c.id_cliente
    and loc.id_locacao = 2;


select
	#loc.*,
    vMod.nome,
    vMar.nome
from
	veiculo_modelo vMod,
    veiculo_marca vMar,
	veiculo v
where
	vMar.id_marca = vMod.id_marca
	and vMod.id_modelo = v.id_modelo;

/*
É possível criar 'views'
Geralmente as views são muito utilizadas para relatórios
As views aparecem na opção 'Views could not be...' no workbench
As views podem ser consideradas tabelas criadas através de junção de informações com critérios...
*/
create view view_locacao as
select 
	loc.*,
    c.nome "nome_cliente",
    v.nome "nome_veiculo",
    md.nome "nome_modelo",
    ma.nome "nome_marca",
    (loc.km_final - loc.km_inicial) km_percorrida
from 
	locacao loc, 
    cliente c,
    veiculo v,
    veiculo_modelo md,
    veiculo_marca ma
where 
	ma.id_marca = md.id_marca
	and md.id_modelo = v.id_modelo;

/*
É possível darmos um select na view
*/
select * from view_locacao
where id_locacao = 8;

/*
Descrever uma view ou tabela
*/
desc view_locacao;

/*
Mostrar oque foi criado!
show create view 'o_que_eu_quero_pesquisar_aqui' (tables)
*/
show create view locacao;

/*
DROP
*/
drop view view_locacao;
#WHERE

# ATIVIDADE
# Lista dos clientes cadastrados, com nome, CPF, celular, cidade e estado
select
	c.nome "nome_cliente",
    c.cpf "cpf_cliente",
    c.celular "celular_cliente",
    cd.nome "nome_cidade_cliente",  -- where c.id_cidade = cd.id_cidade
    es.nome "nome_estado_cliente"  -- where es.id_estado = cd.id_estado
from
	cliente c,
	cidade cd,
    estado es
where
    c.id_cidade = cd.id_cidade
    and es.id_estado = cd.id_estado;
    
select
	c.nome "nome_cliente",
    c.cpf "cpf_cliente",
    c.celular "celular_cliente",
    cd.nome "nome_cidade_cliente",  -- where c.id_cidade = cd.id_cidade
    es.nome "nome_estado_cliente",  -- where es.id_estado = cd.id_estado
    (select count(*) from locacao where id_cliente - c.id_cliente) "qtd_locacoes" -- subquery
from
	cliente c,
	cidade cd,
    estado es
where
    c.id_cidade = cd.id_cidade
    and es.id_estado = cd.id_estado;

# ATIVIDADE
update cliente set id_cidade = 2567 where id_cidade < 777;
update cliente set id_cidade = 2609 where id_cidade < 777;
select * from cliente;
update cliente set id_cidade = 2567 where id_cliente > 6;

# ATIVIDADE
# Lista dos clientes cadastrados para SC, contendo nome, CPF e celular
select
	c.nome "nome_cliente",
    c.cpf "cpf_cliente",
    c.celular "celular_cliente"
from
	cliente c,
	cidade cd,
    estado es
where
    c.id_cidade = cd.id_cidade
    and es.id_estado = cd.id_estado
    and es.sigla = "SC";
    -- and es.id_estado = 24; 
    -- id 24 = SC
    
select
	c.nome "nome_cliente",
    c.cpf "cpf_cliente",
    c.celular "celular_cliente"
from
	cliente c,
	cidade cd,
    estado es
where
    c.id_cidade = cd.id_cidade
    and es.id_estado = cd.id_estado
    and es.sigla = "SC"
order by
	cd.nome asc,
    c.nome asc
